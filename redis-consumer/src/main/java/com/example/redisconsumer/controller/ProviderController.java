package com.example.redisconsumer.controller;

import org.apache.logging.log4j.message.ObjectMessage;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;

@RestController
public class ProviderController {
    @Resource
    StringRedisTemplate stringRedisTemplate;
    @GetMapping("/sendmessage")
    public void sendmessage() throws Exception{
        // 实例化消息生产Producer
        DefaultMQProducer producer=new DefaultMQProducer("producer");
        // 设置NameServer的地址
        producer.setNamesrvAddr("127.0.0.1:9876");
        //启动Producer实例
        producer.start();
        for (int i = 0;i < 10;i++){
            Message msg=new Message("TopicTest","TagA",("Hello word["+i+"]").getBytes(StandardCharsets.UTF_8));
            //发送消息到broker
            SendResult sendResult=producer.send(msg,10000);
            //通过SendResult返回消息判断是否发送成功
            System.out.printf("%s%n",sendResult);
        }

        //如果不在发送消息关闭实例
        producer.shutdown();
    }
}
