package com.java;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Consumer_8010 {
    public static void main(String[] args) {
        SpringApplication.run(Consumer_8010.class,args);
    }
}
