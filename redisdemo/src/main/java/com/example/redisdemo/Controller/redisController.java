package com.example.redisdemo.Controller;

//import com.example.redisdemo.util.RedisUtil;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@RestController
public class redisController {
    @Resource
    StringRedisTemplate stringRedisTemplate;
    @GetMapping("/redis/setvalue")
    public String setvalue(){
        String key="myconfig";
        String value="this is my config";
        //stringRedisTemplate.opsForValue().set(key,value);
        stringRedisTemplate.opsForValue().set(key,value, 10, TimeUnit.SECONDS);
        //RedisUtil su=new RedisUtil();
        //su.set(key,value,10);
        return value;
    }
    @GetMapping("/redis/getvalue")
    public String getvalue(){
        String key="myconfig";
        String a="";
        a=stringRedisTemplate.opsForValue().get(key);
        return a;
    }
}
