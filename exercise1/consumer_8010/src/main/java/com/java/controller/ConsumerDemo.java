package com.java.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class ConsumerDemo {

    @Autowired
    RestTemplate restTemplate;

    private static final String REST_URL_PREFIX = "http://provider";

    @GetMapping("/consumerTest")
    public String test(){
        return "consumer:" + restTemplate.getForObject(REST_URL_PREFIX + "/providerTest",String.class);
    }
}
