package com.java;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Provider_8001 {
    public static void main(String[] args) {
        SpringApplication.run(Provider_8001.class,args);
    }
}
